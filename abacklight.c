#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include <getopt.h>
#include <stdbool.h>
#include <string.h>

#define VERSION "0.1.0"

#define SYSFSDIR     "/sys/class/backlight"
#define INTELDRIVER  "intel_backlight"
#define MODESETTINGS "intel_backlight"

#define PANIC(format, ...)                                                                   \
	do {                                                                                 \
		fprintf(stderr, "%s() line %d: " format, __func__, __LINE__, ##__VA_ARGS__); \
		exit(EXIT_FAILURE);                                                          \
	} while(0)

#define PANIC_PERROR()                           \
	do {                                     \
		PANIC("%s\n", strerror(errno));  \
	} while(0)

void *xmalloc(size_t size) {
	void *ptr = malloc(size);
	if (ptr == NULL) {
		PANIC_PERROR();
	}

	return ptr;
}

void xstrtoul(unsigned long *buf, char *str, int base) {
	char *endptr;

	errno = 0;
	*buf = strtoul(str, &endptr, base);
	if (errno == ERANGE || *endptr != '\0' || endptr == str) {
		PANIC("Invalid string\n");
	}
}

char *freadwhole(FILE *stream) {
	long size = 0;
	char *buf = NULL;

	fseek(stream, 0, SEEK_END);
	size = ftell(stream);
	rewind(stream);

	buf = xmalloc(size);

	fread(buf, size, 1, stream);
	if (ferror(stream)) {
		goto error;
	}

	return buf;

error:
	perror(__func__);
	free(buf);

	return NULL;
}

char *strstrip(char *str) {
	char *start = NULL;
	char *end = NULL;
	size_t len = 0;

	len = strlen(str);
	start = str;
	end = str + len - 1;

	while (isspace(*start)) {
		start++;
		len--;
	}

	while (isspace(*end)) {
		end--;
		len--;
	}

	*(end + 1) = '\0';

	return strndup(start, len);
}

char *read_sysfs_entry(const char *node) {
	char *path = NULL;
	char *buf  = NULL;
	char *res  = NULL;
	FILE *fd   = NULL;
	int r = 0;

	path = xmalloc(PATH_MAX);

	sprintf(path, "%s/%s/%s", SYSFSDIR, INTELDRIVER, node);

	fd = fopen(path, "r");
	if (fd == NULL) {
		goto cleanup;
	}

	buf = freadwhole(fd);
	if (buf == NULL) {
		goto cleanup;
	}

	r = fclose(fd);
	if (r < 0) {
		goto cleanup;
	}

	res = strstrip(buf);

cleanup:
	free(path);
	free(buf);

	return res;
}

int write_sysfs_entry(const char *node, const char *buf) {
	char *path = NULL;
	FILE *fd   = NULL;
	int r = 0;
	size_t len = strlen(buf);

	path = xmalloc(PATH_MAX);

	sprintf(path, "%s/%s/%s", SYSFSDIR, INTELDRIVER, node);

	fd = fopen(path, "w");
	if (fd == NULL) {
		goto error;
	}

	fwrite(buf, len, 1, fd);

	r = fclose(fd);
	if (r < 0) {
		goto error;
	}

	free(path);
	return 0;

error:
	free(path);
	return -1;
}

unsigned long parse_brightness() {
	char *buf = NULL;
	unsigned long res = 0;

	buf = read_sysfs_entry("brightness");
	if (buf == NULL) {
		PANIC_PERROR();
	}

	xstrtoul(&res, buf, 10);

	free(buf);

	return res;
}

unsigned long parse_max_brightness() {
	char *buf = NULL;
	unsigned long res = 0;

	buf = read_sysfs_entry("max_brightness");
	if (buf == NULL) {
		PANIC_PERROR();
	}

	xstrtoul(&res, buf, 10);

	free(buf);

	return res;
}

float get_brightness() {
	return ((float) parse_brightness() / (float) parse_max_brightness()) * 100;
}

void set_brightness(unsigned long val) {
	char *buf = NULL;
	size_t bufsize = 100;
	int r = 0;
	float new_brightness = ((float) val / 100) * (float) parse_max_brightness();

	buf = xmalloc(bufsize);

	snprintf(buf, bufsize, "%.0f", new_brightness);

	r = write_sysfs_entry("brightness", buf);
	if (r < 0) {
		PANIC_PERROR();
	}

	free(buf);
}

void inc_brightness(unsigned long val) {
	float brightness = get_brightness();
	set_brightness(brightness + val);
}

void dec_brightness(unsigned long val) {
	float brightness = get_brightness();
	set_brightness(brightness - val);
}

void usage() {
	fprintf(stderr, "usage: %s ACTION\n", program_invocation_short_name);
	fprintf(stderr, "\n");
	fprintf(stderr, "[a]wesome [backlight] uses sysfs for controlling the backlight\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "actions:\n");
	fprintf(stderr, " --get          Query backlight and print it\n");
	fprintf(stderr, " --set VALUE    Set backlight to VALUE\n");
	fprintf(stderr, " --inc VALUE    Increment backlight by VALUE\n");
	fprintf(stderr, " --dec VALUE    Decrement backlight by VALUE\n");
	fprintf(stderr, " --version      Show version string\n");
	fprintf(stderr, " --help         Show this page and exit\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Since internally getopt_long_only() is used, the action syntax\n");
	fprintf(stderr, "is compatible to xbacklight and can be used with one dash as well.\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Please report bugs to https://github.com/rumpelsepp/abacklight\n");
}

int main(int argc, char *const argv[]) {
	int c = 0;
	unsigned long val = 0;

	const struct option options[] = {
		{ "get",     no_argument,       NULL, 'g' },
		{ "set",     required_argument, NULL, 's' },
		{ "inc",     required_argument, NULL, 'i' },
		{ "dec",     required_argument, NULL, 'd' },
		{ "help",    no_argument,       NULL, 'h' },
		{ "version", no_argument,       NULL, 'V' },
		{  NULL,     0,                 NULL,  0  },
	};

	// Use getopt_long_only() to get support for xbacklight compatibility: -get, -set, ...
	while ((c = getopt_long_only(argc, argv, "", options, NULL)) >= 0) {
		switch (c) {
		case 'g':
			printf("%f\n", get_brightness());
			exit(EXIT_SUCCESS);
		case 's':
			xstrtoul(&val, optarg, 10);
			set_brightness(val);
			exit(EXIT_SUCCESS);
		case 'i':
			xstrtoul(&val, optarg, 10);
			inc_brightness(val);
			exit(EXIT_SUCCESS);
		case 'd':
			xstrtoul(&val, optarg, 10);
			dec_brightness(val);
			exit(EXIT_SUCCESS);
		case 'h':
			usage();
			exit(EXIT_SUCCESS);
		case 'V':
			printf("%s %s\n", program_invocation_short_name, VERSION);
			exit(EXIT_SUCCESS);
		default:
			// Should not get here.
			printf("%f\n", get_brightness());
			exit(EXIT_SUCCESS);
		}
	}

	printf("%f\n", get_brightness());
	return EXIT_SUCCESS;
}
