CFLAGS = -Wall -Wextra
objects := $(patsubst %.c,%.o,$(wildcard *.c))


abacklight : $(objects)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

clean:
	rm -rf abacklight *.o *.s *.i
